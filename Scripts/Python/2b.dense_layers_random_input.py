import pickle
from sklearn.model_selection import train_test_split
import keras
from keras import layers
import pandas as pd

wd = '/home/mk/Dropbox/Active_RStudio_projects/Application_of_CNNs_to_gene_expression_data/'
n_epochs = 250
number_of_convolutional_layers = 1
kernel_size_options = [1,2,3]
filter_size_options = [1,2,4]
n_supergenes_options = range(1,51)
index_options = range(10,31)

for index in index_options:

    for n_supergenes in n_supergenes_options:
        
          for kernel_size in kernel_size_options:
              
              for filter_size in filter_size_options:
              
                  print(n_supergenes)
                  
                  bottleneck = 978 - n_supergenes
                  
                  ### Load data and create train/test set: ###
                  A = open(wd + 'Output/python_input/different_clustering_random/python_input_order_' + str(index) +'.pckl', 'rb')
                  data = pickle.load(A)
                  A.close()
                  x_train, x_test = train_test_split(data, test_size=0.20, random_state=42)
                                                                             
                  input_img = keras.Input(shape = data.shape[1:])
                  
                  x = layers.Dense(data.shape[2] - bottleneck, activation='linear')(input_img)
                  x = layers.Reshape((data.shape[2] - bottleneck, 1, 1), input_shape = (data.shape[2] - bottleneck,))(x)
                       
                  for i in range(1, number_of_convolutional_layers+1):            
                          x = layers.Conv2D(filter_size, (kernel_size, 1), activation='relu', padding='same')(x) 
                  
                  x = layers.Dense(1, activation='linear')(x)
                  x = layers.Reshape((1, data.shape[2] - bottleneck))(x)
                  decoded = layers.Dense(data.shape[2], activation='linear')(x)
                  
                  autoencoder = keras.Model(input_img, decoded)
                  #autoencoder.summary()
                  autoencoder.compile(optimizer='adam', loss='mean_squared_error')
                  
                  
                  result = autoencoder.fit(x_train, x_train,
                                  epochs = n_epochs,
                                  batch_size = 32,
                                  shuffle = True,
                                  validation_data = (x_test, x_test),
                                  verbose = 0
                                  )
                                                      
                  df = pd.DataFrame(data={                
                      'index': index,
                      'n_supergenes': n_supergenes,
                      'number_of_convolutional_layers': number_of_convolutional_layers,
                      'kernel_size': kernel_size,
                      'filter_size': filter_size,
                       'epoch': range(1,n_epochs+1), 
                       'loss': result.history['val_loss']
                      })
                                  
                  filename = wd + 'Output/dense_layer_analysis/random/' + str(n_supergenes) + '_supergenes_index_' + str(index) + '_kernelsize_' + str(kernel_size) + '_filtersize_' + str(filter_size)
                              
                  with open(filename + ".pickle", 'wb') as handle:
                      pickle.dump(df, handle, protocol=pickle.HIGHEST_PROTOCOL)
