import os
import pickle
from sklearn.model_selection import train_test_split
import keras
from keras import layers
import pandas as pd

# Change to correct directory for your project: #
os.getcwd()
os.chdir('/Users/mk/Dropbox/Active_RStudio_projects/CNN_LINCS_genexp')
os.getcwd()

n_epochs = 30
filter_sizes = [32, 64, 128]
kernel_sizes = [3, 5, 7]

used_filter_sizes = []
used_kernel_sizes = []
used_index = []
final_losses = []
current_model = 0

for filter_size in filter_sizes:
    for kernel_size in kernel_sizes:
        for index in range(1,110):
            
            print('Busy with index #'+ str(index))
            A = open('Output/python_input/different_clustering_ordered/python_input_order_' + str(index) +'.pckl', 'rb')
            data = pickle.load(A)
            A.close()
            
            x_train, x_test = train_test_split(data, test_size=0.20, random_state=42)

            input_img = keras.Input(shape=(1, 978, 1))

            x = layers.Conv2D(filter_size, (kernel_size, 1), activation='relu', padding='same')(input_img)
            x = layers.MaxPooling2D((2, 1), padding='same')(x)
            x = layers.Conv2D(8, (3, 1), activation='relu', padding='same')(x)
            x = layers.MaxPooling2D((2, 1), padding='same')(x)
            x = layers.Conv2D(8, (3, 1), activation='relu', padding='same')(x)
            encoded = layers.MaxPooling2D((2, 1), padding='same')(x)
            encoded = x

            x = layers.Conv2D(8, (3, 1), activation='relu', padding='same')(encoded)
            x = layers.UpSampling2D((2, 1))(x)
            x = layers.Conv2D(8, (3, 1), activation='relu', padding='same')(x)
            x = layers.UpSampling2D((2, 1))(x)
            x = layers.Conv2D(filter_size, (3, 1), activation='relu')(x)
            x = layers.UpSampling2D((2, 1))(x)
            decoded = layers.Conv2D(1, (kernel_size, 1), activation='sigmoid', padding='same')(x)

            autoencoder = keras.Model(input_img, decoded)
            autoencoder.compile(optimizer='adam', loss='mean_squared_error')


            result = autoencoder.fit(x_train, x_train,
                            epochs = n_epochs,
                            batch_size = 32,
                            shuffle = True,
                            validation_data = (x_test, x_test))
            
            df = pd.DataFrame(data={                
                'index': index,
                'kernel_size': kernel_size,
                'filter_size': filter_size,
                 'epoch': range(1,n_epochs+1), 
                 'val_loss': result.history['val_loss']
                })
            
            
            filename = 'Output/pre_structuring_analysis/filtersize_' + str(filter_size) + '_kernel_size_' + str(kernel_size) + '_n_epochs_' + str(n_epochs) + 'index_' + str(index) 
                        
            with open(filename + ".pickle", 'wb') as handle:
                pickle.dump(df, handle, protocol=pickle.HIGHEST_PROTOCOL)                        
            
            current_model = current_model + 1
            print('Finished with model #' + str(current_model))
            








